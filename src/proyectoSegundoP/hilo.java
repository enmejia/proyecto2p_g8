/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoSegundoP;

import javafx.application.Platform;
import javafx.scene.control.Label;

/**
 *
 * @author guile
 */
public class hilo implements Runnable{
    private boolean continuar;
    private Label time;

    public hilo( Label time) {
        this.continuar = true;
        this.time = time;
    }
    
    
    

    @Override
    public void run() {
 while (continuar ) {
            try {
                Thread.sleep(1000);
                PantallaJuego.aumentarTiempo();
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        PantallaJuego.actualizar();                   
                    }
                });
                
            } catch (InterruptedException ex) {
                
            }

        }        
    }
    
    public void parar() {
        continuar = false;
    }
    
}
