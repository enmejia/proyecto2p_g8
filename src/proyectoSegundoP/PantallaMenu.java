/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoSegundoP;

/**
 *
 * @author Evelyn
 */

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Evelyn
 */

public class PantallaMenu {
    private StackPane pantalla;
    private Label labelTitulo;
    private Button botonNuevaPartida;
    private Button botonAjustes;
    private Button botonEstadisticas;
    private Label labeletiqueta;
    private Button regresarPartida;
    
    VBox centro;
     ImageView fondo;

    public ImageView getFondo() {
        return fondo;
    }

    public void setFondo(ImageView fondo) {
        this.fondo = fondo;
    }

    public VBox getCentro() {
        return centro;
    }

    public void setCentro(VBox centro) {
        this.centro = centro;
    }
    

    public Button getBotonNuevaPartida() {
        return botonNuevaPartida;
    }

    public void setBotonNuevaPartida(Button botonNuevaPartida) {
        this.botonNuevaPartida = botonNuevaPartida;
    }

    public Button getBotonAjustes() {
        return botonAjustes;
    }

    public void setBotonAjustes(Button botonAjustes) {
        this.botonAjustes = botonAjustes;
    }

    public Button getBotonEstadisticas() {
        return botonEstadisticas;
    }

    public void setBotonEstadisticas(Button botonEstadisticas) {
        this.botonEstadisticas = botonEstadisticas;
    }
    
     public Button getReiniciar() {
        return regresarPartida;
    }

    public void setRegresar(Button reiniciar) {
        this.regresarPartida = reiniciar;
    }
    public PantallaMenu(){
         contenidoMenu();
    }
    public StackPane getPantalla(){
        return pantalla;
    }
    public void contenidoMenu(){
        BorderPane border= new BorderPane();
        labelTitulo = new Label("Roba Montón");
        botonNuevaPartida= new Button("Iniciar nueva partida");
        botonAjustes= new Button("Ajustes");
        botonEstadisticas= new Button("Boton Estadisticas");
        labeletiqueta = new Label("POO - PROYECTO 2P - MEJIA E. - VILLACIS R. - ANDRADE A.");
        regresarPartida= new Button("Regresar a la partida");
        //Vbox de botones  
        centro= new VBox(20);
        centro.getChildren().addAll(botonNuevaPartida, botonAjustes, botonEstadisticas, regresarPartida);
        
        //Alineaciones 
        labelTitulo.setAlignment(Pos.CENTER);
        centro.setAlignment(Pos.CENTER);
        labeletiqueta.setAlignment(Pos.CENTER);
        
        //Tamaño
        labelTitulo.setPrefSize(609, 127);
        labeletiqueta.setPrefSize(609, 13);
        botonNuevaPartida.setPrefSize(175, 67);
        botonAjustes.setPrefSize(175, 67);
        botonEstadisticas.setPrefSize(175, 67);
        regresarPartida.setPrefSize(175, 67);

        
        
        //ubicacion en border
        border.setTop(labelTitulo);
        border.setCenter(centro);
        border.setBottom(labeletiqueta);
        
        
        //Estilo
        labelTitulo.setStyle(  "-fx-text-fill: rgb(194, 178, 54);"+ "-fx-font-family:Lato , sans-serif;"+ "-fx-font-size: 75px;"+ "-fx-font-weight: bold;"+ "-fx-letter-spacing: -1px;"+
                                                "line-height: 1;"+ "text-align: center;"); 
               
        botonNuevaPartida.setStyle(  "-fx-font-size: 15px;"+"-fx-background-color: #F8F8FF;"+"-fx-background-radius:30;"+"-fx-background-insets: 0,1,1;"+
                                    "-fx-text-fill: black;"+ "-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 3, 0.0 , 0 , 1 );"); 
        
        botonAjustes.setStyle(  "-fx-font-size: 15px;"+"-fx-background-color:  #F8F8FF;"+"-fx-background-radius:30;"+"-fx-background-insets: 0,1,1;"+
                                    "-fx-text-fill: black;"+ "-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 3, 0.0 , 0 , 1 );");     
        
        botonEstadisticas.setStyle("-fx-font-size: 15px;"+"-fx-background-color: #F8F8FF;"+"-fx-background-radius:30;"+"-fx-background-insets: 0,1,1;"+
                                    "-fx-text-fill: black;"+ "-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 3, 0.0 , 0 , 1 );"); 
        
        regresarPartida.setStyle("-fx-font-size: 15px;"+"-fx-background-color: #F8F8FF;"+"-fx-background-radius:30;"+"-fx-background-insets: 0,1,1;"+
                                    "-fx-text-fill: black;"+ "-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 3, 0.0 , 0 , 1 );"); 
        

        pantalla= new StackPane();
        fondo=new ImageView(new Image("Recursos/fondo.jpg"));
        pantalla.getChildren().add(fondo);
        pantalla.getChildren().addAll(border);
    }
    
}
