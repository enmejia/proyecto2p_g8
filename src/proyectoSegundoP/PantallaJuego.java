package proyectoSegundoP;

import Modelo.Carta;
import Modelo.Juego;
import Modelo.Jugador;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Evelyn
 */
public class PantallaJuego {
    private static int partidasJugadas=0;
    private static int partidasEmpatadas= 0;
    private static int partidasGanadas = 0;
    private static int puntosTotales=0;
    private static int puntosOpTotales=0;
    private static int diferenciaPuntos=0;
    private static int pilasRobadas=0;
    private static int pilasRobadasT=0;
    private static int diferenciaPuntosT=0;
    private static int puntos=0;
    private static int puntosOponente=0;
    private static String tiempo2;
    
    
    
    private static int[] time;
    private static Label tiempo;
    private static hilo t;
    
    private StackPane pantalla;
    private Label labelTitulo;
   //private Label tiempo;
    private Button regresarMenu;
    ImageView reverso;

    public Button getRegresarMenu() {
        return regresarMenu;
    }

    public void setRegresarMenu(Button opciones) {
        this.regresarMenu = opciones;
    }
    private ArrayList<Label> cartasPersona;
    private ArrayList<Label> cartasPC;
    private ArrayList<Label> cartasMesa;
    private Label montoPersona;
    private Label montoPC;
    private int reparticiones;

    HBox panelPersona;
    VBox contenido;
    HBox barraEstado;
    HBox panelPC;
    ImageView image;
    
    String ruta;
    
    HBox cartasManoPC;
    HBox panelCartasPersona;
    
    public PantallaJuego(Juego juego,PantallaMenu menu){
        this.reparticiones = 0;
        contenidoJuego(juego,menu);
    }
    
    public StackPane getPantalla(){
        return pantalla;
    }

 
    public ArrayList<Label> getCartasPersona() {
        return cartasPersona;
    }

    public void setCartasPersona(ArrayList<Label> cartasPersona) {
        this.cartasPersona = cartasPersona;
    }

    public ArrayList<Label> getCartasPC() {
        return cartasPC;
    }

    public void setCartasPC(ArrayList<Label> cartasPC) {
        this.cartasPC = cartasPC;
    }

    public ArrayList<Label> getCartasMesa() {
        return cartasMesa;
    }

    public void setCartasMesa(ArrayList<Label> cartasMesa) {
        this.cartasMesa = cartasMesa;
    }

    public Label getMontoPersona() {
        return montoPersona;
    }

    public void setMontoPersona(Label montoPersona) {
        this.montoPersona = montoPersona;
    }

    public Label getMontoPC() {
        return montoPC;
    }

    public void setMontoPC(Label montoPC) {
        this.montoPC = montoPC;
    }
        StackPane cambio;
    BorderPane border;

    
    
    public void contenidoJuego(Juego juego,PantallaMenu menu){
        time=new int[2];
        
        
        tiempo=new Label();
      
        t = new hilo(tiempo);
        Thread hT = new Thread(t);
        hT.start();
       //Vbox que contendrá todo el contenido de la escena
       juego.reiniciarJuego();
        contenido= new VBox();

        BorderPane border= new BorderPane();
        
        //Barra Estado
            barraEstado= new HBox();
            labelTitulo = new Label("Partida en juego");
            tiempo= new Label("1:1");
            regresarMenu= new Button();
            barraEstado.getChildren().addAll(tiempo, labelTitulo, regresarMenu);

            //Alineaciones 
            labelTitulo.setAlignment(Pos.CENTER);
            tiempo.setAlignment(Pos.CENTER);
            regresarMenu.setAlignment(Pos.CENTER_RIGHT);

            //Tamaño
            labelTitulo.setPrefSize(441, 31);
            tiempo.setPrefSize(87, 31);
            regresarMenu.setPrefSize(21, 23);

        
            //Estilo
        
            labelTitulo.setStyle(  "-fx-text-fill: #F5FFFA;"+ "-fx-font-family:Lato , sans-serif;"+ "-fx-font-size: 18px;"+ "-fx-font-weight: bold;"+ "-fx-letter-spacing: -1px;"+
                                                "line-height: 1;"+ "text-align: center;"); 
            tiempo.setStyle(  "-fx-text-fill: #F5FFFA;"+ "-fx-font-family:Lato , sans-serif;"+ "-fx-font-size: 18px;"+ "-fx-font-weight: bold;"+ "-fx-letter-spacing: -1px;"+
                                                "line-height: 1;"+ "text-align: center;"); 
        
        
  
       //BORDERPANE DESARROLLO JUEGO
      
   /*    regresarMenu.setOnAction(e -> {
            panelOpciones(juego,menu);
            
        }
        );*/ 

        //PANEL PERSONA   
             
            panelPersona= new HBox();
            panelCartasPersona= new HBox();
            cartasPersona = new ArrayList<>();
            montoPersona= new Label();
            panelPersona.getChildren().addAll(montoPersona,panelCartasPersona);

            //MONTO JUGADOR
            formaPanelMano(juego,panelCartasPersona,cartasPersona);

            agregarImagenMontoJ(juego);
            agregarImagenManoJ(juego);

            /*
            for(int i=0;i<cartasPersona.size();i++){
            if (cartasPersona.indexOf(cartasPersona.get(i))==0){
                HBox.setMargin(cartasPersona.get(i), new Insets(10,50,5,10));
            }if (cartasPersona.indexOf(cartasPersona.get(i))>0){
                HBox.setMargin(cartasPersona.get(i), new Insets(10,8,5,8));
            }
            
            cartasMesa = new ArrayList<>();
            panelCartasPersona.setAlignment(Pos.CENTER);
            panelCartasPersona.getChildren().add(cartasPersona.get(i));
            }*/
            
            
            border.setBottom(panelPersona);
            
            // RECORREMOS
//            int repeticiones=1;

             for(int i=0; i<cartasPersona.size();i++){
                 
                Label labelJ = cartasPersona.get(i);
                labelJ.setOnMouseClicked((e)-> {
                    labelJ.setDisable(true);
                        
                    
                    String ruta = labelJ.getId();
                    
                    Carta cartaReal= null;
                    for(Carta carta:juego.getPersona().getCartasMano()){
                        if(carta.getRutaCarta().equalsIgnoreCase(ruta)){
                            cartaReal = carta;
                        }
                        
                    }
                    
                    
                     
                    int val = juego.getPersona().revisarCarta(juego.getCartasMesa(), juego.getCpu().getCartasMonto(), cartaReal);
                    
                    if(val==0){
                        juego.getPersona().ponerCartaMesa(juego.getCartasMesa(), cartaReal);
                        agregarImagenMesa(juego);
                        labelJ.setGraphic(null);
                        
                    }
                    if(val==1){
                        juego.getPersona().robarCartaMesa(juego.getCartasMesa(), cartaReal);
                        agregarImagenMesa(juego);
                        agregarImagenMontoJ(juego);
                        cartasMesa.get(juego.getCartasMesa().size()).setGraphic(null);
                        labelJ.setGraphic(null);
                        
                        
                    }
                    if(val == 2){
                        juego.getPersona().robarCartaComputador(juego.getCpu(), cartaReal);
                        pilasRobadas+= 1;
                        agregarImagenMontoJ(juego);
                        labelJ.setGraphic(null);
                        montoPC.setGraphic(null);
                        
                        
                    }                    
                    turnoComputadora(juego.getCartasMesa(), juego.getCpu().getCartasMano(), juego.getPersona().getCartasMonto(),juego);
                   //CONDICIONAL CUANDO SE ACABAN LAS CARTAS -----------------------------------------------------
                        
                   if(juego.getCpu().getCartasMano().isEmpty()){
                            for(Label c:cartasPersona){
                                c.setDisable(false);
                            }
                            

                            System.out.println("SE HAN ACABADO LAS CARTAS3");
                            reparticiones+=1;
                            juego.repartirCartasJugador();
                            agregarImagenManoC(juego);                       
                            agregarImagenManoJ(juego);      
                           

                             System.out.println("Mano jugador------------------");
                             for(Carta carta:juego.getPersona().getCartasMano() ){
                                System.out.println(carta.getNumeroCarta());

                            }
                            System.out.println("Mano CPU------------------");
                            for(Carta carta2:juego.getCpu().getCartasMano() ){
                                System.out.println(carta2.getNumeroCarta());

                            }
                            System.out.println("Mesa------------------");
                            for(Carta carta2:juego.getCartasMesa() ){
                                System.out.println(carta2.getNumeroCarta());
                            }
                            
                        }

                        if(juego.getPersona().getCartasMonto().isEmpty()){
                            montoPersona.setGraphic(null);
                            
                            
                        }
                    
                if ((reparticiones==8)){

                        try {
                            System.out.println("JUEGO TERMINADO");
                            stop();
                            //TOTAL PARTIDAS
                            partidasJugadas++;
                            if(juego.getPersona().getCartasMonto().size()==juego.getCpu().getCartasMonto().size()){
                                partidasEmpatadas++;
                            }
                            if(juego.getPersona().getCartasMonto().size()>juego.getCpu().getCartasMonto().size()){
                                partidasGanadas++;
                            }
                            puntosTotales+= juego.getPersona().getCartasMonto().size();
                            puntosOpTotales+=juego.getCpu().getCartasMonto().size();
                            pilasRobadasT += pilasRobadas;
                            diferenciaPuntosT = puntosTotales-puntosOpTotales;
                            
                            //ULTIMA PARTIDA
                            puntos = juego.getPersona().getCartasMonto().size();
                            puntosOponente = juego.getCpu().getCartasMonto().size();
                            diferenciaPuntos = puntos - puntosOponente;
                            tiempo2 = Integer.toString(time[0]);
                            
                            
                            
                            
                            System.out.println("----partidasJugadas");
                            System.out.println(partidasJugadas);
                            //NO VALIO METODO DAR PUNTAJE-HICE SET
                            int montoCpu=(juego.getCpu().getCartasMonto().size());
                            juego.getCpu().setPuntaje(montoCpu);
                            
                            int montoPersona=(juego.getPersona().getCartasMonto().size());
                            juego.getPersona().setPuntaje(montoPersona);
                            //System.out.println("CPU punteaje seteado"+juego.getCpu().getPuntaje());
                            // System.out.println(""Persona"+punteaje seteado"+juego.getPersona().getPuntaje());
                            //System.out.println("Mesa"+juego.getCartasMesa().size());
                            
                            juego.getCpu().getPuntaje();
                            juego.getPersona().getPuntaje();
                            juego.darCartasAlGanador();
                            
                            //System.out.println("LUEGO DE APLICAR METODO DARCARTASGANADOR");
                            //System.out.println("CPU punteaje seteado"+juego.getCpu().getPuntaje());
                            // System.out.println(""Persona"+punteaje seteado"+juego.getPersona().getPuntaje());
                            //System.out.println("Mesa"+juego.getCartasMesa().size());
                            //YA VALE(:
                        } catch (Exception ex) {
                            Logger.getLogger(PantallaJuego.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    System.out.println("JUEGO TERMINADO");
              
               
                    //NO VALIO METODO DAR PUNTAJE-HICE SET   
                    int montoCpu=(juego.getCpu().getCartasMonto().size());
                    juego.getCpu().setPuntaje(montoCpu);
                   
                     int montoPersona=(juego.getPersona().getCartasMonto().size());
                    juego.getPersona().setPuntaje(montoPersona);
                     //System.out.println("CPU punteaje seteado"+juego.getCpu().getPuntaje());
                   // System.out.println(""Persona"+punteaje seteado"+juego.getPersona().getPuntaje()); 
                    //System.out.println("Mesa"+juego.getCartasMesa().size());
                                        
                    juego.getCpu().getPuntaje();
                    juego.getPersona().getPuntaje();
                    juego.darCartasAlGanador();
                            for(Label c:cartasPersona){
                                c.setDisable(true);
                            }
                    
                    //System.out.println("LUEGO DE APLICAR METODO DARCARTASGANADOR");
                     //System.out.println("CPU punteaje seteado"+juego.getCpu().getPuntaje());
                   // System.out.println(""Persona"+punteaje seteado"+juego.getPersona().getPuntaje()); 
                    //System.out.println("Mesa"+juego.getCartasMesa().size());
                    //YA VALE(:

                    
                }   
                });
                
            
            
            }
        
        //PANEL PC     
            panelPC= new HBox();
            montoPC= new Label();
            HBox cartasManoPC= new HBox();
            cartasPC = new ArrayList<>();


            //MONTO PC
            formaPanelMano(juego,cartasManoPC,cartasPC);

            agregarImagenManoC(juego);
         
            agregarImagenMontoC(juego);
         
            
            panelPC.getChildren().addAll(montoPC,cartasManoPC);
            border.setTop(panelPC);

        //PANEL MESA      
                    
            GridPane panelMesa= new GridPane();
            cartasMesa = new ArrayList<>();
            panelMesa.setHgap(5);
            panelMesa.setVgap(5);
         //MESA
            formaPanelMesa(panelMesa,cartasMesa,juego);  
           // System.out.println(cartasMesa.size()+"cartas en Mesa luego de formar panel");
            agregarImagenMesa(juego);

            border.setCenter(panelMesa);
       
          
            
            
            
        contenido.getChildren().addAll(barraEstado, border);
        pantalla= new StackPane();
        ImageView fondo=new ImageView(new Image("Recursos/FondoPartida.jpg"));
        pantalla.getChildren().add(fondo);
        pantalla.getChildren().addAll(contenido);
        
        
        
        }
  
            
        public void agregarImagenMontoJ(Juego juego){
            montoPersona.setStyle("-fx-border-width: 1;"+ "-fx-border-style: solid;"+"-fx-border-color:white; ");

            int tamañoMonto=juego.getPersona().getCartasMonto().size();
            if (tamañoMonto>0){
                int posUltimaCartaMonto=tamañoMonto-1;
                String ruta2 = (juego.getPersona().getCartasMonto().get(posUltimaCartaMonto).getRutaCarta());
                ImageView im=new ImageView(new Image(ruta2));
                im.setFitWidth(94);
                im.setFitHeight(135);
                montoPersona.setGraphic(im);

                montoPersona.setPrefSize(94, 135);

                }else{
                    montoPersona.setPrefSize(108, 142);

                }
        }
        
        public void agregarImagenMontoC(Juego juego){
        montoPC.setStyle("-fx-border-width: 1;"+ "-fx-border-style: solid;"+"-fx-border-color:white; ");

        int tamañoMonto=juego.getCpu().getCartasMonto().size();
        if (tamañoMonto>0){
            int posUltimaCartaMonto=tamañoMonto-1;
            String ruta = (juego.getCpu().getCartasMonto().get(posUltimaCartaMonto).getRutaCarta());
            ImageView im=new ImageView(new Image(ruta));
            im.setFitWidth(94);
            im.setFitHeight(135);
            montoPC.setGraphic(im);
            montoPC.setPrefSize(94, 135);

            }else{
                montoPC.setPrefSize(108, 142);
                
            }
        }
        
        public void agregarImagenManoJ(Juego juego){
       // System.out.println("");
        
            for(int i=0;i<juego.getPersona().getCartasMano().size();i++){
            
        ruta = (juego.getPersona().getCartasMano().get(i).getRutaCarta());
       // System.out.println("RUTA DE IMGEN(DEBERIA SER LA MISMA DEL ID LABEL)");
       // System.out.println(ruta); 
        image = new ImageView(new Image(ruta));            
        image.setFitWidth(94);
        image.setFitHeight(135);
        cartasPersona.get(i).setStyle("-fx-border-width: 1;"+ "-fx-border-style: solid;"+"-fx-border-color:white; ");

       	cartasPersona.get(i).setGraphic(image);
        cartasPersona.get(i).setId(ruta);
        
    
        }
       //     System.out.println("");
        for(Label l:cartasPersona){
              //   System.out.println("ID CARTAS JUGADOR PC AL REINICIARSE");
                // System.out.println(l.getId());
             }
        }
            
        public void agregarImagenManoC(Juego juego ){
       // System.out.println("");
        for(int i=0;i<juego.getCpu().getCartasMano().size();i++){
        ruta = (juego.getCpu().getCartasMano().get(i).getRutaCarta());
                
            // System.out.println("RUTA DE IMGEN(DEBERIA SER LA MISMA DEL ID LABEL)");
            //System.out.println(ruta);
        reverso= new ImageView(new Image("mazoPoker/back1.png"));
        image = new ImageView(new Image(ruta));            
        image.setFitWidth(94);
        image.setFitHeight(135);
        cartasPC.get(i).setStyle("-fx-border-width: 1;"+ "-fx-border-style: solid;"+"-fx-border-color:white; ");

       	cartasPC.get(i).setGraphic(image);
          
        cartasPC.get(i).setId(ruta);
        reverso.setFitWidth(94);
        reverso.setFitHeight(135);
                	cartasPC.get(i).setGraphic(reverso);

        }
        //m.out.println("");
           for(Label l:cartasPC){
               //  System.out.println("ID CARTAS PC AL REINICIARSE");
                 //System.out.println(l.getId());
             }
        }
       
         public void formaPanelMano(Juego juego,HBox contendorCartasMano, ArrayList<Label> labelsMano){
             for(int i=0;i<3;i++){
                Label label=new Label();
                label.setPrefSize(94, 135);    
                label.setStyle("-fx-border-width: 1;"+ "-fx-border-style: solid;"+"-fx-border-color:white; ");
                HBox.setMargin(label, new Insets(10,12,5,12));
                contendorCartasMano.getChildren().add(label);
                labelsMano.add(label);
                contendorCartasMano.setAlignment(Pos.CENTER);
            
            }
            
        }
        public void formaPanelMesa(GridPane gridPane, ArrayList<Label> labels,Juego juego){
                     for(int i=0;i<12;i++){
                Label label= new Label();
                label.setStyle("-fx-border-width: 1;"+ "-fx-border-style: solid;"+"-fx-border-color:white; ");

                label.setPrefSize(90, 130);              
                if(i<6) {
                gridPane.add(label, i, 0);
                labels.add(label);
                }
                if(i>=6) {
                gridPane.add(label, i-6, 1);
                labels.add(label);
                }              
                gridPane.setAlignment(Pos.CENTER);
            }
        

                
                gridPane.setAlignment(Pos.CENTER);    
        
    }
    public void agregarImagenMesa(Juego juego){
        
     for(int i=0;i<juego.getCartasMesa().size();i++){
            String ruta = (juego.getCartasMesa().get(i).getRutaCarta());
            image = new ImageView(new Image(ruta));            
            image.setFitWidth(90);
            image.setFitHeight(130);
            cartasMesa.get(i).setGraphic(image);
        }          
    }
        
         public void turnoComputadora(ArrayList<Carta> mesa,ArrayList<Carta> mano,ArrayList<Carta> montoEnemigo,Juego juego){
            Label labelC=null;
            Carta cartaC = null;
            
            cartaC = juego.getCpu().robarMontoPersona(mano,montoEnemigo);
            if(cartaC!=null){
                for(Label label : cartasPC){
                    if(cartaC.getRutaCarta().equalsIgnoreCase(label.getId())){
                        labelC = label;
                        labelC.setGraphic(null);
                    }
                    agregarImagenMontoJ(juego);
                    agregarImagenMontoC(juego);
                }
            }
            if(cartaC==null){
                cartaC = juego.getCpu().robarCartaMesa(mesa, mano);
                if(cartaC!=null){
                    for(Label label : cartasPC){
                        if(cartaC.getRutaCarta().equals(label.getId())){
                            labelC = label;
                           labelC.setGraphic(null);

                        }
                    }agregarImagenMesa(juego);
                    agregarImagenMontoC(juego);
                    cartasMesa.get(juego.getCartasMesa().size()).setGraphic(null);
                    
                }if(cartaC==null){
                    cartaC = juego.getCpu().colocarCartaMesa(mano, mesa);
                    for(Label c : cartasPC){
                        if(c.getId().equals(cartaC.getRutaCarta())){
                            labelC=c;
                           labelC.setGraphic(null);
                        }

                    }
                    agregarImagenMesa(juego);
            
            
                }
            }
                     
            
            
            
        }
         public void reiniciarPantalla(Juego juego){
             reparticiones=0;
           for (Label l:cartasPC){
               //l.setId(null);
               l.setGraphic(null);
               
           }
         //  System.out.println(cartasPC.size()+"cartasPC");

           for (Label l:cartasPersona){
               //l.setId(null);
               l.setGraphic(null);
               l.setDisable(false);
           }
           for (Label l:cartasMesa){
               //l.setId(null);
               l.setGraphic(null);
           }
           montoPC.setGraphic(null);
           montoPersona.setGraphic(null);
           System.out.println();
           
           
      // cartasPC.clear();
      //  cartasPersona.clear();
    //cartasMesa.clear();
        agregarImagenMesa(juego);
        agregarImagenManoJ(juego);
        agregarImagenManoC(juego);
        agregarImagenMontoJ(juego);
        agregarImagenMontoC(juego);
        
       }
         public static String getTiempo() {
        if (time[0] > 9) {
            return "0" + time[1] + ":" + time[0];
        } else {
            return "0" + time[1] + ":0" + time[0];
        }
    }
     public static void aumentarTiempo() {
        if (time[0] == 59) {
            time[0] = 00;
            time[1]++;
        } else {
            time[0]++;
        }
    }
     
    public static void actualizar(){
        tiempo.setText(getTiempo());
    }
    

       
    /*    public void panelOpciones(Juego juego,PantallaMenu menu){
        cambio= new StackPane();
        
        VBox vbox= new VBox();
        vbox= menu.getCentro();
        Button regresar= new Button("Regresar a partida");
        regresar.setStyle("-fx-font-size: 15px;"+"-fx-background-color: #F8F8FF;"+"-fx-background-radius:30;"+"-fx-background-insets: 0,1,1;"+
                                    "-fx-text-fill: black;"+ "-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 3, 0.0 , 0 , 1 );"); 
        regresar.setPrefSize(175, 67);

        StackPane.setAlignment(regresar, Pos.CENTER);
        
        
        menu.getBotonNuevaPartida().setOnAction(e->{
        
        juego.reiniciarJuego();
        reiniciarPantalla(juego);
        for(Label c:cartasPersona){
            c.setDisable(false);
        }        
       // for(Carta c:juego.getPersona().getCartasMano()){
        //System.out.print(c.getNumeroCarta()+"");
        //}          
            
        cambio.setVisible(false);
            
        });
        
        regresar.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                cambio.setVisible(false);
                //cambio.getChildren().add(border);
            }
        });
        
        vbox.getChildren().add(regresar);
        cambio.getChildren().addAll(vbox);
        cambio.setStyle("-fx-background-color: rgba(0, 100, 100, 0.5); -fx-background-radius: 10;");
         //contenido= new VBox();
       //contenido.getChildren().addAll(cambio);        
        pantalla.getChildren().add(cambio);
        
         
                
        } 
        
        
     */

    public static void setTime(int[] time) {
        PantallaJuego.time = time;
    }
    
    public static void stop() throws Exception{
        t.parar();
    }


    public static int getPartidasJugadas() {
        return partidasJugadas;
    }

    public void ayuda(Label label){
        label.setStyle("-fx-border-width: 5;"+ "-fx-border-style: solid;"+"-fx-border-color:yellow; ");
    }
    
    
    public int cartaMarcomesa(ArrayList<Carta>mesa, ArrayList<Carta>monto,ArrayList<Carta>mano){
    int n=0;
    for(Carta c:mesa ){
        for(int i=0;i<mano.size();i++){
            if(c.getNumeroCarta().equals(mano.get(i).getNumeroCarta())){
                if(n<1){
                   n=1;
                }
            
            }

        }

    }
    for(int i=0;i<mano.size();i++){
        if(monto.size()>0){
        if(n<2){
            if(monto.get(monto.size()-1).getNumeroCarta().equals(mano.get(i).getNumeroCarta())){
            n=2;
            }
    
    }
    
            
            }
    }
        return n ;
        }

    public ArrayList darPosicion(ArrayList<Carta>mesa,ArrayList<Carta>mano){
        ArrayList<Integer> l=new ArrayList<Integer>();
        int n=0;
    int pmano=0;
    int pmesa=0;
    for(int j=0;j<mesa.size();j++ ){
        for(int i=0;i<mano.size();i++){
            if(mesa.get(j).getNumeroCarta().equals(mano.get(i).getNumeroCarta())){
                if(n<1){
                   n=1;
                   pmesa=j;
                   pmano=i;
                }
            
            }

        }

    }        
       l.add(pmesa);
       l.add(pmano);
    
    
    
    return l;
    }


    
    
    }
    
   
        
    
    







        

    
    
    
    
    
    
    

