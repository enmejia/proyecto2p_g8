/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoSegundoP;

import Modelo.Juego;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.geometry.Insets;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Evelyn
 */
public class PantallaAjustes {
    Button cartaEspañola;
    Label etiquetaEspañola;
    Button cartaPoker;
    Label etiquetaTitulo;
    Label etiquetaPoker;
    StackPane pantalla;
    Button regresar;
    CheckBox ayuda;
    

    public Button getRegresar() {
        return regresar;
    }

    public void setRegresar(Button regresar) {
        this.regresar = regresar;
    }

    public PantallaAjustes(Juego juego) {
         contenidoAjustes(juego);
        
    }
    public PantallaAjustes(Button cartaEspañola, Button cartaPoker) {
        this.cartaEspañola = cartaEspañola;
        this.cartaPoker = cartaPoker;
    }
   
    public StackPane getPantalla(){
        return pantalla;
    }
    
    public void contenidoAjustes(Juego juego){
        BorderPane border= new BorderPane();
HBox hbox= new HBox();
        regresar=new Button();
        etiquetaTitulo=new Label("Escoja su mazo");
        etiquetaTitulo.setStyle("-fx-text-fill: #1f4b70;"+ "-fx-font-family:Lato , sans-serif;"+ "-fx-font-size: 27px;"+ "-fx-font-weight: bold;"+ "-fx-letter-spacing: -1px;"+
                                                "line-height: 1;"+ "text-align: center;");

        hbox.getChildren().addAll(etiquetaTitulo,regresar);
        border.setTop(hbox);
        etiquetaTitulo.setPrefSize(614,  120);
        etiquetaTitulo.setAlignment(Pos.CENTER);
        
        VBox centro=new VBox(50);
        HBox tiposCarta= new HBox(50);
        cartaPoker= new Button();
        String ruta1 = ("mazoPoker/back1.png");
            ImageView imagen1=new ImageView(new Image(ruta1));
            imagen1.setFitWidth(178);
            imagen1.setFitHeight(258);
            cartaPoker.setGraphic(imagen1);
            cartaPoker.setPrefSize(178, 258);
   
        cartaEspañola= new Button();
        String ruta2 = ("mazoPoker/9.png");
            
            ImageView imagen2=new ImageView(new Image(ruta2));
            imagen2.setFitWidth(178);
            imagen2.setFitHeight(258);
            cartaEspañola.setGraphic(imagen2);
            cartaEspañola.setPrefSize(94, 135);
        
        tiposCarta.setAlignment(Pos.CENTER);
            
        HBox etiquetasCarta= new HBox(170);
        etiquetaPoker=new Label("ESPAÑOL");
        etiquetaPoker.setStyle("-fx-text-fill: #000000;"+ "-fx-font-family:Lato , sans-serif;"+ "-fx-font-size: 20px;"+ "-fx-font-weight: bold;"+ "-fx-letter-spacing: -1px;"+
                                                "line-height: 1;"+ "text-align: center;");    
            
        etiquetaEspañola=new Label("POKER");
        etiquetaEspañola.setStyle("-fx-text-fill: #000000;"+ "-fx-font-family:Lato , sans-serif;"+ "-fx-font-size: 20px;"+ "-fx-font-weight: bold;"+ "-fx-letter-spacing: -1px;"+
                                                "line-height: 1;"+ "text-align: center;");    
        
        etiquetasCarta.setAlignment(Pos.CENTER);
               cartaEspañola.setOnAction(e->{
                        juego.TIPO_DE_CARTA="Espanola";
            try {
                BufferedWriter doc1=new BufferedWriter(new FileWriter("ajuste.txt"));
                doc1.write("Espanola");
                doc1.close();
            } catch (IOException ex) {
                Logger.getLogger(PantallaAjustes.class.getName()).log(Level.SEVERE, null, ex);
            }                        
                        juego.reiniciarJuego();
                        
                        

        });
        
        
        cartaPoker.setOnAction(e->{
            Juego.TIPO_DE_CARTA="Poker";
            try {

                BufferedWriter doc1=new BufferedWriter(new FileWriter("ajuste.txt"));
                doc1.write("Poker");
                doc1.close();
            } catch (IOException ex) {
                Logger.getLogger(PantallaAjustes.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
       
        ayuda= new CheckBox("Pulse la casilla si desea activar/desactivar la pista de carta");
        ayuda.setText("OLIBOLI");
        
        
        border.setBottom(ayuda);
        BorderPane.setAlignment(ayuda, Pos.CENTER);
        BorderPane.setMargin(ayuda, new Insets(30,0,50,0));
        
        
        
        etiquetasCarta.getChildren().addAll(etiquetaPoker,etiquetaEspañola );
        tiposCarta.getChildren().addAll(cartaEspañola, cartaPoker);
        centro.getChildren().addAll(tiposCarta, etiquetasCarta);
        border.setCenter(centro);
        
        pantalla= new StackPane();
        ImageView fondo=new ImageView(new Image("Recursos/32583.jpg"));
        fondo.setFitHeight(630);
        fondo.setFitWidth(640);
        pantalla.getChildren().add(fondo);
        pantalla.getChildren().addAll(border);
        
        
        
    }
    
    
    
    
    
    
    
    
    
}
