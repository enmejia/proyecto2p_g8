/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoSegundoP;

/**
 *
 * @author Evelyn
 */
import Modelo.Juego;
import static Modelo.Juego.TIPO_DE_CARTA;

import java.util.logging.Level;
import java.util.logging.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Evelyn
 */
public class Main extends Application {
        Juego juego= new Juego();
        PantallaMenu panelMenu;
        PantallaJuego panelJuego;
        PantallaAjustes panelAjustes;
        PantallaEstadisticas panelEstadisticas;
    
    @Override
    public void start(Stage primaryStage) throws FileNotFoundException, IOException {
        FileReader doc=new FileReader("ajuste.txt");
        BufferedReader doc1=new BufferedReader(doc);
        String linea=doc1.readLine();
            System.out.print(linea);
            TIPO_DE_CARTA=linea;
        
        

         panelMenu= new PantallaMenu();
         panelJuego= new PantallaJuego(juego,panelMenu);
        panelAjustes= new PantallaAjustes(juego);
         panelEstadisticas= new PantallaEstadisticas(juego);
        
        
        
        panelMenu.getReiniciar().setVisible(false);
        
        
        StackPane contenedor1 = panelMenu.getPantalla();
        StackPane contenedor2 = panelJuego.getPantalla();
        StackPane contenedor3 = panelAjustes.getPantalla();
        StackPane contenedor4 = panelEstadisticas.getPantalla();


        
        //crear escena
        
        Scene scene1 = new Scene(contenedor1, 630, 640); 
        primaryStage.setScene(scene1);
        
        
        Scene scene2 = new Scene(contenedor2, 630, 640); 
        panelMenu.getBotonNuevaPartida().setOnAction(e -> {
            int[] time = new int[2];
            //titulo = "Nueva Partida";
            PantallaJuego.setTime(time);
            
            juego.reiniciarJuego();
            
            
            
            panelJuego.reiniciarPantalla(juego);
            
            
            
            primaryStage.setScene(scene2);
        }
        );

        Scene scene3 = new Scene(contenedor3, 630, 640);
        panelMenu.getBotonAjustes().setOnAction(e -> {
            //titulo = "Ajustes";
            
            primaryStage.setScene(scene3);
            
        }
        );
        
        
        Scene scene4 = new Scene(contenedor4, 630, 640);
        panelMenu.getBotonEstadisticas().setOnAction(e -> {
            //titulo = "Ajustes";
            panelEstadisticas.refrescarpantalla();
            primaryStage.setScene(scene4);
        }
        );
  
        
        panelAjustes.getRegresar().setOnAction(e -> {
            primaryStage.setScene(scene1);

        });
        
        panelJuego.getRegresarMenu().setOnAction(e -> {
            panelMenu.getReiniciar().setVisible(true);
            panelMenu.getReiniciar().setOnAction(u -> {
            
                
            primaryStage.setScene(scene2);
});
            primaryStage.setScene(scene1);
        });;
  
        
        primaryStage.setTitle("PROYECTO SEGUNDO PARCIAL");
      //  scene.getStylesheets().add(getClass().getResource("estiloMenu.css").toExternalForm());

        primaryStage.show();
        
            
    }
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
        try {
                PantallaJuego.stop();
            } catch (Exception ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
   
               
           
    
}
