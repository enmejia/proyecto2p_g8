/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoSegundoP;

import Modelo.Juego;
import java.util.ArrayList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import static proyectoSegundoP.PantallaJuego.getTiempo;

/**
 *
 * @author Evelyn
 */
public class PantallaEstadisticas {
    Label   tituloUltimaPartida,tituloTotalPartidas,etiqueta_puntos, etiqueta_puntosOponente, etiqueta_diferenciaPuntos, etiqueta_tiempo,etiqueta_tiempoMedio,
            etiqueta_pilasRobadas,etiqueta_partidasJugadas,etiqueta_partidasGanadas,etiqueta_partidasEmpatadas,
            etiqueta_porcentajeVictorias,puntos, puntosOponente, diferenciaPuntos, tiempo,tiempoMedio,
            pilasRobadas,partidasJugadas,partidasGanadas,partidasEmpatadas, porcentajeVictorias,etiqueta_puntosU,puntosU;
    Label etiquetaTitulo;
 
    ArrayList<Label> etiquetasUltimaPartida= new ArrayList();
    ArrayList<Label> valoresUltimaPartida= new ArrayList();
    ArrayList<Label> etiquetasTodasPartida= new ArrayList();
    ArrayList<Label> valoresTodasPartida= new ArrayList();
    
    
    Modelo.Juego juego= new Modelo.Juego();
    StackPane pantalla;
    
    public PantallaEstadisticas(Juego juego){
         contenidoEstadisticas(juego);
    }
    public StackPane getPantalla(){
        return pantalla;
    }

    public void contenidoEstadisticas(Juego juego){
        String n=getTiempo();
        etiqueta_puntos= new Label("Puntos");                           puntos= new Label(Integer.toString(juego.getPersona().getPuntaje()));
        etiqueta_puntosOponente= new Label("Puntos Oponente");          puntosOponente= new Label(Integer.toString(juego.getCpu().getPuntaje()));
        etiqueta_diferenciaPuntos= new Label("Diferencia de puntos");   diferenciaPuntos= new Label(Integer.toString(Math.abs(juego.getPersona().getPuntaje()-juego.getCpu().getPuntaje())));
        etiqueta_tiempo= new Label("Tiempo");                           tiempo= new Label(n);  
        etiqueta_pilasRobadas= new Label("Pilas robadas");              pilasRobadas= new Label(Integer.toString(juego.getPersona().getPilasRobadas()));       
              

        
        etiquetasUltimaPartida.add(etiqueta_puntos);etiquetasUltimaPartida.add(etiqueta_puntosOponente);
        etiquetasUltimaPartida.add(etiqueta_diferenciaPuntos);etiquetasUltimaPartida.add(etiqueta_tiempo);
        etiquetasUltimaPartida.add(etiqueta_pilasRobadas);
        
        valoresUltimaPartida.add(puntos);valoresUltimaPartida.add(puntosOponente);valoresUltimaPartida.add(diferenciaPuntos);
        valoresUltimaPartida.add(tiempo);valoresUltimaPartida.add(pilasRobadas);
               
        VBox vbox=new VBox();
        etiquetaTitulo=new Label("ESTADISTICAS");
        etiquetaTitulo.setStyle("-fx-text-fill: #1f4b70;"+ "-fx-font-family:Lato , sans-serif;"+ "-fx-font-size: 27px;"+ "-fx-font-weight: bold;"+ "-fx-letter-spacing: -1px;"+
                                                "line-height: 1;"+ "text-align: center;");
        etiquetaTitulo.setAlignment(Pos.CENTER);
        etiquetaTitulo.setPrefSize(630, 30);
        tituloUltimaPartida=new Label("ULTIMA PARTIDA");
        tituloUltimaPartida.setAlignment(Pos.CENTER);
        tituloUltimaPartida.setPrefSize(630, 30);
        
        GridPane gridUltimaP= new GridPane();
        gridUltimaP.setHgap(12);
        gridUltimaP.setVgap(12);
        
        for(Label etiquetaU:etiquetasUltimaPartida){
        gridUltimaP.addColumn(0, etiquetaU);
        
//88gridUltimaP.getColumnConstraints().add(new ColumnConstraints(100)); // column 0 is 100 wide
        //gridUltimaP.getColumnConstraints().add(new ColumnConstraints(200)); // column 1 is 200 wide
        GridPane.setHalignment(etiquetaU, HPos.LEFT);
        GridPane.setMargin(etiquetaU, new Insets(5,395,5,30));

        }
        for(Label valorU:valoresUltimaPartida){
        gridUltimaP.addColumn(1, valorU);
        GridPane.setHalignment(valorU, HPos.RIGHT);

        }
        
        
        
        
        
        etiqueta_puntos= new Label("Puntos");                               puntos= new Label("asd");
        etiqueta_puntosOponente= new Label("Puntos Oponente");              puntosOponente= new Label("asd");
        etiqueta_diferenciaPuntos= new Label("Diferencia de puntos");       diferenciaPuntos= new Label("ada");
        etiqueta_tiempoMedio= new Label("Tiempo medio de partida");         tiempoMedio= new Label("we");
        etiqueta_pilasRobadas= new Label("Pilas robadas");                  pilasRobadas= new Label("ae");
        etiqueta_partidasJugadas= new Label("Partidas Jugadas");            partidasJugadas= new Label("ae");
        etiqueta_partidasGanadas= new Label("Partidas Ganadas");            partidasGanadas= new Label("ae");
        etiqueta_partidasEmpatadas= new Label("Partidas Empatadas");        partidasEmpatadas= new Label("asf"); 
        etiqueta_porcentajeVictorias= new Label("Porcentaje de victorias"); porcentajeVictorias= new Label("er");
        
               
        etiquetasTodasPartida.add(etiqueta_puntos);etiquetasTodasPartida.add(etiqueta_puntosOponente);
        etiquetasTodasPartida.add(etiqueta_diferenciaPuntos);etiquetasTodasPartida.add(etiqueta_partidasJugadas);
        etiquetasTodasPartida.add(etiqueta_partidasGanadas);etiquetasTodasPartida.add(etiqueta_partidasEmpatadas);
        etiquetasTodasPartida.add(etiqueta_porcentajeVictorias);etiquetasTodasPartida.add(etiqueta_tiempoMedio);
        etiquetasTodasPartida.add(etiqueta_pilasRobadas);
       
        
        valoresTodasPartida.add(puntos);valoresTodasPartida.add(puntosOponente);valoresTodasPartida.add(diferenciaPuntos);
        valoresTodasPartida.add(partidasJugadas);valoresTodasPartida.add(partidasGanadas);valoresTodasPartida.add(partidasEmpatadas);
        valoresTodasPartida.add(porcentajeVictorias);valoresTodasPartida.add(tiempoMedio);
        valoresTodasPartida.add(pilasRobadas);
        
     
        
        tituloTotalPartidas=new Label("TOTAL PARTIDAS");
        tituloTotalPartidas.setAlignment(Pos.CENTER);
        tituloTotalPartidas.setPrefSize(630, 30);
        GridPane gridTotalP= new GridPane();
        gridTotalP.setHgap(12);
        gridTotalP.setVgap(12);
        for(Label etiqueta:etiquetasTodasPartida){
        gridTotalP.addColumn(0, etiqueta);
        GridPane.setHalignment(etiqueta, HPos.LEFT);
        GridPane.setMargin(etiqueta, new Insets(5,375,5,30));

        }
        for(Label valor:valoresTodasPartida){
        gridTotalP.addColumn(1, valor);
        GridPane.setHalignment(valor, HPos.RIGHT);

        }
        
        vbox.getChildren().addAll(etiquetaTitulo,tituloUltimaPartida,gridUltimaP,tituloTotalPartidas,gridTotalP);
        pantalla= new StackPane();
        pantalla.getChildren().addAll(vbox);
    
    }
    
    public void refrescarpantalla(){
        
         String n=getTiempo();
        etiqueta_puntos= new Label("Puntos");                           puntos= new Label(Integer.toString(juego.getPersona().getPuntaje()));
        etiqueta_puntosOponente= new Label("Puntos Oponente");          puntosOponente= new Label(Integer.toString(juego.getCpu().getPuntaje()));
        etiqueta_diferenciaPuntos= new Label("Diferencia de puntos");   diferenciaPuntos= new Label(Integer.toString(Math.abs(juego.getPersona().getPuntaje()-juego.getCpu().getPuntaje())));
        etiqueta_tiempo= new Label("Tiempo");                           tiempo= new Label(n);  
        etiqueta_pilasRobadas= new Label("Pilas robadas");              pilasRobadas= new Label(Integer.toString(juego.getPersona().getPilasRobadas()));       
              
    
    
    
    
    
    
    }
        
        
        
        
        
        
}



