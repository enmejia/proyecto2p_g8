/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author guile
 */
public class Jugador {
    ArrayList<Carta> cartasMano;
    ArrayList<Carta> cartasMonto;
    int puntaje;

    public Jugador(ArrayList<Carta> cartasMano, ArrayList<Carta> cartasMonto, int puntaje) {
        this.cartasMano = cartasMano;
        this.cartasMonto = cartasMonto;
        this.puntaje = puntaje;
    }

    public Jugador() {
    }
    

    public ArrayList<Carta> getCartasMano() {
        return cartasMano;
    }

    public void setCartasMano(ArrayList<Carta> CartasMano) {
        this.cartasMano = CartasMano;
    }

    public ArrayList<Carta> getCartasMonto() {
        return cartasMonto;
    }

    public void setCartasMonto(ArrayList<Carta> CartasMonto) {
        this.cartasMonto = CartasMonto;
    }

    public int getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }
    public void robarCartaMesa(ArrayList<Carta> mesa,Carta carta){

    }
    public void robarCartaJugador(ArrayList<Carta> monto,Carta carta){

    }
    
    public void darPuntaje(){
        puntaje=cartasMonto.size();
    
    
    
    }
    public int revisarCarta(ArrayList<Carta> mesa,ArrayList<Carta>monto,Carta carta){
    int n=0;//te da cero si no encuentra nada ni en maso ni en el monto del otro jugador
    
    for(Carta m:mesa){
           if(m.getNumeroCarta().equals(carta.getNumeroCarta())) {
               if(n<1){
                   n=1;//te regresa un 1 si encontro en la mesa
               }
           
           }
    
            if(monto.size()!=0){
           if(monto.get(monto.size()-1).getNumeroCarta().equals(carta.getNumeroCarta())) {
               if(n<2){
                   n=2;//te da dos si encuentra en el monto
               
           
               }  
    
    }
    
    }
    
    }
    return n;
}
    public void reiniciarJugador(){
        cartasMano.clear();
        cartasMonto.clear();
        puntaje=0;
    
    }
}
