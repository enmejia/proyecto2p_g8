/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author guile
 */
public class Juego {
    
    private ArrayList<Carta> cartasMesa;
    private Baraja baraja;
    private Persona persona;
    private Computador cpu;
    public static String TIPO_DE_CARTA="Poker";
    

    public Juego() {
        baraja= new Baraja(TIPO_DE_CARTA);
        persona= new Persona(0,0,0,0,new ArrayList<Carta>(),new ArrayList<Carta>(),0);
        cpu=new Computador(new ArrayList<Carta>(),new ArrayList<Carta>(),0);
        if(TIPO_DE_CARTA.equals("Poker")){
        repartirCartasMesa();
        
        }
        
        repartirCartasJugador();
        
        
    }

    public ArrayList<Carta> getCartasMesa() {
        return cartasMesa;
    }

    public void setCartasMesa(ArrayList<Carta> cartasMesa) {
        this.cartasMesa = cartasMesa;
    }
    

    public Baraja getBaraja() {
        return baraja;
    }

    public void setBaraja(Baraja baraja) {
        this.baraja = baraja;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Computador getCpu() {
        return cpu;
    }

    public void setCpu(Computador cpu) {
        this.cpu = cpu;
    }
    

    
    
     /**
     * Reparte cartas aleatorias a los jugadores
     */
    public void repartirCartasJugador(){
        Iterator<Carta> it=baraja.getCartas().iterator();
        int n=0;
        while(it.hasNext()){
            Carta ca=(Carta)it.next();
            if(n<3){
                persona.getCartasMano().add(ca);
                it.remove();
                n++;
            }
            else if((n>=3)&&(n<6)){
                cpu.getCartasMano().add(ca);
                it.remove();
                n++;
            
            
            }
        }
    }
    
         /**
     * Reparte cartas aleatorias a la mesa
     */
        public void repartirCartasMesa(){
        cartasMesa=new ArrayList<Carta>();
        Iterator<Carta> it=baraja.getCartas().iterator();
        int n=0;
        while(it.hasNext()){
            Carta ca=it.next();
            if(n<4){
                cartasMesa.add(ca);
                it.remove();
                n++;
            }
        }
    }
        
         /**
     * Da cartas al ganador
     */
        public void darCartasAlGanador(){
        if(persona.getPuntaje()> cpu.getPuntaje()){
            Iterator<Carta> it=cartasMesa.iterator();
            while(it.hasNext()){
                Carta c=it.next();
                persona.getCartasMonto().add(c);
                it.remove();
            
            
            }
            
        
        }
        else if(cpu.getPuntaje()>persona.getPuntaje()){
            Iterator<Carta> it=cartasMesa.iterator();
                while(it.hasNext()){
                    Carta c=it.next();
                    cpu.getCartasMonto().add(c);
                    it.remove();
            
            
            }       
        
        
        
        
        }
        
        
        }
        public void reiniciarJuego(){ //cuadno este al comienzo
            
            baraja.reiniciarBaraja();
            persona.reiniciarJugador();
            cpu.reiniciarJugador();
            cartasMesa.clear();
            if(TIPO_DE_CARTA.equals("Poker")){
            repartirCartasMesa();
            }
            
            repartirCartasJugador();
            
        
        
        }
        

        public static void main(String[] args) {
        Juego j= new Juego();
        for (Carta e:j.cartasMesa){
            System.out.println(e.getNumeroCarta()+e.getSimboloCarta());
        
         }   j.reiniciarJuego();
        for (Carta c:j.cartasMesa){
            System.out.println(c.getNumeroCarta()+c.getSimboloCarta());
        }
                   // System.out.println(j.numeroDeReparticiones());
}

        
        
        
    }
        
    
    
    


    
    

