/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Evelyn
 */
public class Carta {

    /**
     * Nombre de la carta.
     */
    private String numeroCarta;

    /**
     * Ruta de la carta.
     */
  
    private String rutaCarta;
    /**
     * Simbolo de la carta.
     */
    private String simboloCarta;
   
    
    
    
    /**
     * Crea un pais, guardando su nombre y capital.
     * @param nombreC Nombre de la carta. 
     * @param rutaC Ruta de la imagen de la carta.
     */
    
    
    public Carta(String numeroC, String rutaC, String simboloC ){
        simboloCarta = simboloC;
        numeroCarta = numeroC;
        rutaCarta = rutaC;
    }
    
    

    public String getNombreCarta() {
        return numeroCarta;
    }

    public void setNombreCarta(String nombreCarta) {
        this.numeroCarta = nombreCarta;
    }

    public String getRutaCarta() {
        return rutaCarta;
    }

    public void setRutaCarta(String rutaCarta) {
        this.rutaCarta = rutaCarta;
    }

    public String getNumeroCarta() {
        return numeroCarta;
    }

    public void setNumeroCarta(String numeroCarta) {
        this.numeroCarta = numeroCarta;
    }

    public String getSimboloCarta() {
        return simboloCarta;
    }

    public void setSimboloCarta(String simboloCarta) {
        this.simboloCarta = simboloCarta;
    }
    

}