/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 *
 * @author guile
 */
public class Computador extends Jugador {
    
    public Computador(ArrayList<Carta> CartasMano, ArrayList<Carta> CartasMonto, int puntaje) {
        super(CartasMano, CartasMonto, puntaje);
    }

    public ArrayList<Carta> getCartasMano() {
        return cartasMano;
    }

    public void setCartasMano(ArrayList<Carta> CartasMano) {
        this.cartasMano = CartasMano;
    }

    public ArrayList<Carta> getCartasMonto() {
        return cartasMonto;
    }

    public void setCartasMonto(ArrayList<Carta> CartasMonto) {
        this.cartasMonto = CartasMonto;
    }

    public int getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }
    /**
     * Roba una carta de la mesa 
     * @param mesa : el array de las cartas que se encuentran en mesa
     * @param mano : el array de las cartas que se encuentran en la mano de la computadora
     * @return un objeto de tipo Carta
     */
    public Carta robarCartaMesa(ArrayList<Carta> mesa,ArrayList<Carta> mano){
            Carta carta = null;
            boolean con = true;
            if(mesa.isEmpty()==false){    
                if(mano.isEmpty()==false){
                 
                    Iterator<Carta> ot =mano.iterator();
                    int n=0;
                    
                    while(ot.hasNext()){
                
                        Carta cartaMano = (Carta) ot.next();
                        
                        Iterator<Carta> it =mesa.iterator();
                        while(it.hasNext()){
                            Carta cartaMesa =(Carta) it.next();
                          //  System.out.println("Carta mesa iterado");
                            if(n<1){
                                
                                if(cartaMesa.getNumeroCarta().equalsIgnoreCase(cartaMano.getNumeroCarta())){
                                    System.out.println("Metodo robar carta mesa Ejecutado");
                                    cartasMonto.add(cartaMesa);
                                    cartasMonto.add(cartaMano);
                                    carta = cartaMano;
                                    ot.remove();
                                    it.remove();
                                    n++;  
                                    con=false;
                                }
                            }
                        }
                    }

                }
            }return carta;            
    }
    
    
     /**
     * Roba una carta de la la pila del jugador contrario 
     * @param montoEnemigo : el array de las cartas que se encuentran en monto del jugador contrario
     * @param mano : el array de las cartas que se encuentran en la mano de la computadora
     * @return un objeto de tipo Carta
     */
    public Carta robarMontoPersona(ArrayList<Carta> mano,ArrayList<Carta> montoEnemigo){
        Carta carta=null;
        Iterator<Carta> it1 =mano.iterator();
        boolean con = true;
        //System.out.println("Entro al metodo robar monto persona de Computador");
        while(it1.hasNext()){
            Carta cartaMano = (Carta) it1.next();
            if(montoEnemigo.isEmpty()==false){
                if(cartaMano.getNumeroCarta().equalsIgnoreCase(montoEnemigo.get(montoEnemigo.size()-1).getNumeroCarta())){
                    carta = cartaMano;
                    System.out.println("Metodo robar monto persona ejecutado");
                    cartasMonto.addAll(montoEnemigo);
                    montoEnemigo.clear();
                    it1.remove();
                    con=false;
                }    
            }
        }return carta;
    }
    
     /**
     * Coloca una carta en la mesa
     * @param mesa : el array de las cartas que se encuentran en mesa
     * @param mano : el array de las cartas que se encuentran en la mano de la computadora
     * @return un objeto de tipo Carta
     */
    public Carta colocarCartaMesa(ArrayList<Carta> mano,ArrayList<Carta> mesa){
        Carta carta=null;
        if(mano.isEmpty()==false){        
           System.out.println("Entro el metodo colocarCartaMesa de Computador");
            Random rd=new Random();
            Carta cartaRandom = mano.get(rd.nextInt(mano.size()));
            carta = cartaRandom;
            mesa.add(cartaRandom);
            mano.remove(cartaRandom);
        }
        return carta;
    }
}
