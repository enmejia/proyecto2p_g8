/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;
import java.util.Iterator;
import static java.util.Spliterators.iterator;

/**
 *
 * @author guile
 */
public class Persona extends Jugador {
    private int partidasJugadas;
    private int partidasGanadas;
    private int partidasPerdidas;
    private int pilasRobadas;

    public Persona(int partidasJugadas, int partidasGanadas, int partidasPerdidas, int pilasRobadas, ArrayList<Carta> cartasMano, ArrayList<Carta> cartasMonto, int puntaje) {
        super(cartasMano, cartasMonto, puntaje);
        this.partidasJugadas = partidasJugadas;
        this.partidasGanadas = partidasGanadas;
        this.partidasPerdidas = partidasPerdidas;
        this.pilasRobadas = pilasRobadas;
    }
    

    
    public Persona(ArrayList<Carta> cartasMano, ArrayList<Carta> cartasMonto, int puntaje) {
        super(cartasMano, cartasMonto, puntaje);
        
    }

    
    
    
    public int getPartidasJugadas() {
        return partidasJugadas;
    }

    public void setPartidasJugadas(int partidasJugadas) {
        this.partidasJugadas = partidasJugadas;
    }

    public int getPartidasGanadas() {
        return partidasGanadas;
    }

    public void setPartidasGanadas(int partidasGanadas) {
        this.partidasGanadas = partidasGanadas;
    }

    public int getPartidasPerdidas() {
        return partidasPerdidas;
    }

    public void setPartidasPerdidas(int partidasPerdidas) {
        this.partidasPerdidas = partidasPerdidas;
    }

    public int getPilasRobadas() {
        return pilasRobadas;
    }

    public void setPilasRobadas(int pilasRobadas) {
        this.pilasRobadas = pilasRobadas;
    }

    public ArrayList<Carta> getCartasMano() {
        return cartasMano;
    }

    public void setCartasMano(ArrayList<Carta> CartasMano) {
        this.cartasMano = CartasMano;
    }

    public ArrayList<Carta> getCartasMonto() {
        return cartasMonto;
    }

    public void setCartasMonto(ArrayList<Carta> CartasMonto) {
        this.cartasMonto = CartasMonto;
    }

    public int getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }
    
     /**
     * Roba una carta de la mesa 
     * @param mesa : el array de las cartas que se encuentran en mesa
     * @param carta : un objeto de tipo Carta 
     */
    public void robarCartaMesa(ArrayList<Carta> mesa,Carta carta){
        agregarCartaMesaaMonto(mesa,carta);
        agregarCartaManoaMonto(carta);
        pilaRobada();
    }
        public void robarCartaComputador(Computador cp,Carta carta){
            if(cp.getCartasMonto().get(cp.getCartasMonto().size()-1).getNumeroCarta().equals(carta.getNumeroCarta())){
                Iterator<Carta> it = cp.getCartasMonto().iterator();
                while(it.hasNext()){
                    cartasMonto.add(it.next());
                    it.remove();
                
                }
            
            
            }
            agregarCartaManoaMonto(carta);
            pilaRobada();
    }
         /**
     * Coloca una carta de la mesa 
     * @param mesa : el array de las cartas que se encuentran en mesa
     * @param carta : un objeto de tipo Carta
     */
        public void ponerCartaMesa(ArrayList<Carta> mesa,Carta carta){
                mesa.add(carta);
                Iterator<Carta> it1= cartasMano.iterator();
                while(it1.hasNext()){
                    Carta cart1=(Carta)it1.next();
                    if((cart1.getNombreCarta().equals(carta.getNombreCarta())&& (cart1.getSimboloCarta().equals(carta.getSimboloCarta())))){
                        it1.remove();
                    
                    }
                
                }                                                    
        
        }
        
         /**
        * Coloca la carta de la mano a su monto      
        * @param carta : un objeto de tipo Carta
        */
        public void agregarCartaManoaMonto(Carta carta){
            Iterator<Carta> it1=cartasMano.iterator();
            int n=0;
            while(it1.hasNext()){
                Carta cart=(Carta)it1.next();
                if(n<1){
                if(cart.getNumeroCarta().equals(carta.getNumeroCarta())){
                    cartasMonto.add(cart);
                    it1.remove();
                    n++;
                
                }
                }
            }          
        
        }
        
         /**
     * Coloca la carta robada en mesa a su monto
     * @param mesa : el array de las cartas que se encuentran en mesa
     * @param carta : un objeto de tipo Carta
     */
        public void agregarCartaMesaaMonto(ArrayList<Carta> mesa,Carta carta){
            Iterator<Carta> it =mesa.iterator();
            int n=0;
            while(it.hasNext()){
                Carta cart=(Carta)it.next();
                if(n<1){
                if(cart.getNumeroCarta().equals(carta.getNumeroCarta())){
                    cartasMonto.add(cart);
                    it.remove();
                    n++;
                
                }
                }
            }
        
        
        
        
        }
        public void pilaRobada(){
            pilasRobadas++;
        
        
        }
    
}
