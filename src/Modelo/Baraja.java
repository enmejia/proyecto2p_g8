/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 *
 * @author guile
 */
public class Baraja {
    
    private String tipo;
    private ArrayList<Carta> cartas;

    public Baraja(String tipo) {
        
        inicializarBaraja(tipo);
    }

     /**
     * Inicializa el tipo de baraja a usar 
     * @param tipo : un String del tipo de baraja 
     */    
    public void inicializarBaraja(String tipo){
        if(tipo.equals("Poker")){
            cartas= new ArrayList<Carta>();
            inicializarBarajaPoker();
                
        }
        if(tipo.equals("Espanola")){
            cartas= new ArrayList<Carta>();
            inicializarBarajaEspañola();

        
        }
    }
    
    
    
    
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
                               
   
    public ArrayList<Carta> getCartas() {
        return cartas;
    }

    public void setCartas(ArrayList<Carta> cartas) {
        this.cartas = cartas;
    }
   
     /**
     * Reinicia la Baraja
     */
    public void reiniciarBaraja(){
        cartas.clear();
        if (Juego.TIPO_DE_CARTA.equals("Poker")){
            inicializarBarajaPoker();
        }
        if (Juego.TIPO_DE_CARTA.equals("Espanola")){
            inicializarBarajaEspañola();
        }            
    }
    
     /**
     * Inicializa la baraja Poker
     */
    public void inicializarBarajaPoker(){
        int i=0;
        int j=0;
        for(int n=0;n<52;n++){
            i=n+1;
            String numero=Integer.toString(i);
            String ruta="mazoPoker/"+numero+".png";
            if(n<=12){
                Carta carta=new Carta(numero,ruta,"Trebol");
                cartas.add(carta);
            }
            else if((n>12) && (n<=25)){
                j=Math.abs(12-n);
                String numero2=Integer.toString(j);
                Carta carta=new Carta(numero2,ruta,"Espada");
                cartas.add(carta);
            }
        
            else if((n>25) && (n<=38)){
                j=Math.abs(25-n);
                String numero2=Integer.toString(j);
                Carta carta=new Carta(numero2,ruta,"Corazon");
                cartas.add(carta);
            }
          
            else if((n>38) && (n<52)){
                j=Math.abs(38-n);
                String numero2=Integer.toString(j);
                Carta carta=new Carta(numero2,ruta,"Diamante");
                cartas.add(carta);
            }             
        
        }    
        generarCartasAleatorias(); 
    
    
    
    }
    
     /**
     * Inicializa la Baraja española
     */
    public void inicializarBarajaEspañola(){
            int i=0;
            int j=0;
            for(int n=0;n<48;n++){
                i=n+1;
                String numero=Integer.toString(i);
                String ruta="mazoEspañol/"+numero+".png";
                if(n<=11){
                    Carta carta=new Carta(numero,ruta,"Bastos");
                    cartas.add(carta);
                }
                else if((n>11) && (n<=23)){
                    j=Math.abs(11-n);
                    String numero2=Integer.toString(j);
                    Carta carta=new Carta(numero2,ruta,"Copas");
                    cartas.add(carta);
                }
        
                else if((n>23) && (n<=35)){
                    j=Math.abs(23-n);
                    String numero2=Integer.toString(j);
                    Carta carta=new Carta(numero2,ruta,"Espadas");
                    cartas.add(carta);
                }
          
                else if((n>35) && (n<48)){
                    j=Math.abs(35-n);
                    String numero2=Integer.toString(j);
                    Carta carta=new Carta(numero2,ruta,"Oros");
                    cartas.add(carta);
            }             
        
        }            
        generarCartasAleatorias();  
    
    
    
    
    
    }
    
     /**
     * Genera cartas aleatoria
     */
    public void generarCartasAleatorias(){
        Collections.shuffle(cartas);
    
    }
   
   
   
}
